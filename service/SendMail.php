<?php

namespace app\Service;

use Yii;

abstract class SendMail
{
    public static function send($email, $uniqkey, $name)
    {
        Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
            ->setReplyTo([$email => $name])
            ->setSubject("subject")
            ->setTextBody("Для активации вашего аккаунта, пожалуйста, перейдите по <a href=" . Yii::$app->request->hostInfo . "web/index.php?r=site%2Factivate&uniqkey=$uniqkey" . ">ссылке<\/a>")
            ->send();
        return true;
    }
}