<?php

namespace app\controllers;

use app\models\SignupForm;
use app\models\User;
use app\models\UserForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->getId();
            $user = User::find()
                ->where(['id' => $userId])
                ->one();
            $model = new UserForm();
            $model->name = $user->name;
            $model->last_name = $user->last_name;

            if($model->load(\Yii::$app->request->post()) && $model->validate()) {
                $user->name = $model->name;
                $user->last_name = $model->last_name;
                $user->save();
                $message = "Данные успешно обновлены: ". $model->name ." ".$model->last_name;
            }

            $deleteLink = Yii::$app->request->hostInfo."/web/index.php?r=site%2Fdelete&id=".$userId;
            return $this->render('index', [
                'model' => $model,
                'message' => $message,
                'deleteLink' => $deleteLink
            ]);
        }
        else {
            return $this->redirect(Yii::$app->request->hostInfo.'/web/index.php?r=site%2Fsignup');
        }

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionSignup($message = null)
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->password == $model->check_password){
                if ($user = $model->signup()) {
                    $message = "Вы успешно зарегистрировались. Активируйте ваш аккаунт по ссылке в вашей e-maill";
                }
            }
            else {
                $message = "Вы не подтвердили пароль. Повторите попытку";
            }
        }
        return $this->render('signup', [
            'model' => $model,
            'message' => $message
        ]);
    }

    public function actionActivate($uniqkey)
    {
        $user = User::find()
            ->where(['uniqkey' => $uniqkey])
            ->one();
        $user->status = 10;
        $user->uniqkey = null;
        $user->save();
        Yii::$app->user->login($user);

        return $this->render('activateSuccess');
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest) {
            $user = User::findOne($id);
            Yii::$app->user->logout();
            $user->delete();
            return $this->goHome();
        }
    }
}
