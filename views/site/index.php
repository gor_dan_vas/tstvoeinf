<?php

/** @var yii\web\View $this */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$this->title = 'Тестовое';
?>
<div class="site-index">
    <div class="body-content">

    <? if ($message != null) {?>
        <div class="alert alert-success" role="alert">
            <?=$message?>
        </div>
    <? }?>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'last_name') ?>
    <?= $form->field($model, 'name') ?>
    <div>
        <div>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end() ?>
    <br />
    <a class="btn btn-lg btn-danger" href="<?=$deleteLink?>">Удалить</a>






    </div>
</div>
