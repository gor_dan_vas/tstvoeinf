<?php

namespace app\models;

use app\Service\SendMail;

class SignupForm extends \yii\base\Model
{
    public $username;
    public $name;
    public $last_name;
    public $email;
    public $password;
    public $check_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['last_name', 'trim'],
            ['last_name', 'required'],
            ['last_name', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['check_password', 'required'],
            ['check_password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->name = $this->name;
        $user->last_name = $this->last_name;
        $user->setPassword($this->password);
        $user->uniqkey = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyz'), 0, 20);
        $user->status = 0;
        $user->generateAuthKey();
        SendMail::send($user->email, $user->uniqkey, $user->name);
        return $user->save() ? $user : null;
    }

    public function attributeLabels() {
        return [
            'last_name' => 'Фамилия',
            'name' => 'Имя',
            'email' => 'Эл. почта',
            'password' => 'Пароль',
            'check_password' => 'Повторите пароль'
        ];
    }


}