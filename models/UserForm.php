<?php

namespace app\models;

class UserForm extends \yii\base\Model
{
    public $name;
    public $last_name;

    public function rules() {
        return [
            [['name', 'last_name'], 'required', 'message' => 'Не оставляйте поле пустым'],
        ];
    }

    public function attributeLabels() {
        return [
            'last_name' => 'Фамилия',
            'name' => 'Имя',
        ];
    }
}